load --init ../spl/spl_progs/S16/S16_init_GCD.xsm
load --idle ../spl/spl_progs/S16/S13_idle.xsm
load --int=6 ../spl/spl_progs/S16/S16_int6_read.xsm
load --int=7 ../spl/spl_progs/S16/S15_int7_write.xsm
load --int=10 ../spl/spl_progs/S16/S14_int10.xsm
load --int=timer ../spl/spl_progs/S16/S14_timer.xsm
load --int=console ../spl/spl_progs/S16/S16_console.xsm
load --module 0 ../spl/spl_progs/S16/S15_mod0_res_man_terminal_handling.xsm
load --module 4 ../spl/spl_progs/S16/S16_mod4_device_manager.xsm
load --module 7 ../spl/spl_progs/S16/S16_mod7_boot.xsm
load --os ../spl/spl_progs/S16/S13_os_startup.xsm

