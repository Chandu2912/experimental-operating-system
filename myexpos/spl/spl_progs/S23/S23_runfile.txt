Previous stages

load --int=console ../spl/spl_progs/S22/S22_A1/S16_console.xsm
load --int=disk ../spl/spl_progs/S22/S22_A1/S18_disk_int_handler.xsm
load --exhandler ../spl/spl_progs/S22/S22_A1/S19_exhandler.xsm
load --int=6 ../spl/spl_progs/S22/S22_A1/S16_int6_read.xsm
load --int=7 ../spl/spl_progs/S22/S22_A1/S15_int7_write.xsm
load --int=8 ../spl/spl_progs/S22/S22_A1/S22_int_8.xsm
load --int=9 ../spl/spl_progs/S22/S22_A1/S19_int9_exec.xsm
load --int=10 ../spl/spl_progs/S22/S22_A1/S20_int10_exit.xsm
load --int=11 ../spl/spl_progs/S22/S22_A1/S21_int11.xsm
load --int=13 ../spl/spl_progs/S22/S22_A1/S22_int13.xsm
load --int=14 ../spl/spl_progs/S22/S22_A1/S22_int14.xsm


load --module 1 ../spl/spl_progs/S22/S22_A1/S22_mod1_process_manager.xsm
load --module 2 ../spl/spl_progs/S22/S22_A1/S19_mod2_memory_manager.xsm

load --module 5 ../spl/spl_progs/S22/S22_A1/S20_mod5_scheduler.xsm

load --os ../spl/spl_progs/S22/S22_A1/S21_os_startup.xsm
load --init ../spl/spl_progs/S22/S22_A1/S21_init_shell.xsm
load --idle ../spl/spl_progs/S22/S22_A1/S13_idle.xsm
load --library ../expl/library.lib
load --int=timer ../spl/spl_progs/S22/S22_A1/S14_timer.xsm


----------------------------------------------------------

S23 modified files

load --int=4 ../spl/spl_progs/S23/int_4.xsm
load --int=15 ../spl/spl_progs/S23/S23_int15_shutdown.xsm
load --module 0 ../spl/spl_progs/S23/S23_mod0_res_man.xsm
load --module 4 ../spl/spl_progs/S23/S23_mod4_device_manager.xsm
load --module 7 ../spl/spl_progs/stage23/mod7.xsm



----------------------------------------------------------------------


exec files to be loaded

load --exec ../spl/spl_progs/S23/create.xsm
load --exec ../spl/spl_progs/S23/delete.xsm

----------------------------------------------------------------------


