load --idle ../spl/spl_progs/S20/S13_idle.xsm
load --int=console ../spl/spl_progs/S20/S16_console.xsm
load --int=timer ../spl/spl_progs/S20/S14_timer.xsm
load --int=disk ../spl/spl_progs/S20/S18_disk_int_handler.xsm
load --int=6 ../spl/spl_progs/S20/S16_int6_read.xsm
load --int=7 ../spl/spl_progs/S20/S15_int7_write.xsm
load --int=8 ../spl/spl_progs/S20/S20_int8_fork.xsm
load --int=9 ../spl/spl_progs/S20/S19_int9_exec.xsm
load --int=10 ../spl/spl_progs/S20/S20_int10_exit.xsm
load --module 0 ../spl/spl_progs/S20/S18_mod0_res_man.xsm
load --module 1 ../spl/spl_progs/S20/process_manager.xsm
load --module 2 ../spl/spl_progs/S20/S19_mod2_memory_manager.xsm
load --module 4 ../spl/spl_progs/S20/S18_mod4_device_manager.xsm
load --module 5 ../spl/spl_progs/S20/S20_mod5_scheduler.xsm
load --module 7 ../spl/spl_progs/S20/S20_mod7_boot.xsm
load --os ../spl/spl_progs/S20/S13_os_startup.xsm
load --exhandler ../spl/spl_progs/S20/S19_exhandler.xsm

load --init ../spl/spl_progs/S20/a1_init.xsm
load --init ../spl/spl_progs/S20/a2_init.xsm

-------------------------------------------------------------------

load --exec ../spl/spl_progs/S20/odd.xsm
load --exec ../spl/spl_progs/S20/Even.xsm

------------------------------------------------------------------












